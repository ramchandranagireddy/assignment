import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {
  constructor(private http: HttpClient) { }

  equipmentsUrl = 'http://localhost:8080/equipments';

  getEquipments() {
    return this.http.get(this.equipmentsUrl);
  }
}
