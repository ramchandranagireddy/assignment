package com.lab.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class EquipmentController {
	
	@RequestMapping("/")
	public String healthCheck() {
		return "OK";
	}
	

	@RequestMapping("/equipments")
	public List<Equipment> getEquipments() {
		List<Equipment> equipments = new ArrayList<Equipment>();
		equipments.add(new Equipment("Equipment1", "Vendor1", "Location1", "Model1", "Serial1", "Vendor1 Desc", null));
		equipments.add(new Equipment("Equipment2", "Vendor2", "Location2", "Model2", "Serial2", "Vendor2 Desc", null));
		equipments.add(new Equipment("Equipment3", "Vendor3", "Location3", "Model3", "Serial3", "Vendor3 Desc", null));
		equipments.add(new Equipment("Equipment4", "Vendor4", "Location4", "Model4", "Serial4", "Vendor4 Desc", null));
		equipments.add(new Equipment("Equipment5", "Vendor5", "Location5", "Model5", "Serial5", "Vendor5 Desc", null));
		equipments.add(new Equipment("Equipment6", "Vendor6", "Location6", "Model6", "Serial6", "Vendor6 Desc", null));
		return equipments;
	}
	
}
